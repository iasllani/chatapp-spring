package unit;

import chatapp.TokenHandler;
import chatapp.entities.chatroom.ChatRoom;
import chatapp.entities.dto.message.NewMessageTransport;
import chatapp.entities.message.Message;
import chatapp.entities.message.MessageException;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.MessageRepo;
import chatapp.repos.UserRepo;
import chatapp.services.MessageService;
import chatapp.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTests {

    @Spy
    UserRepo userRepo;

    @Spy
    @InjectMocks
    UserService userService = new UserService();

    @Spy
    MessageRepo messageRepo;

    @InjectMocks
    MessageService messageService;

    @Test(expected = MessageException.class)
    public void add_null_message_request() throws MessageException, UserException {
        messageService.addMessage(null);
    }

    @Test(expected = MessageException.class)
    public void add_empty_message_request() throws MessageException, UserException {
        messageService.addMessage(new NewMessageTransport("    "));
    }

    @Test(expected = MessageException.class)
    public void add_null_content_message_request() throws MessageException, UserException {
        messageService.addMessage(new NewMessageTransport(null));
    }

    @Test(expected = MessageException.class)
    public void add_longer_than_256_length_content_message_request() throws MessageException, UserException {
        String content = "";
        while (content.length() <= 256)
            content += "1";
        messageService.addMessage(new NewMessageTransport(content));
    }

    @Test
    public void valid_add_message_request() throws MessageException, UserException {
        User user = new User("username", "password");
        TokenHandler.setToken(user.getToken());
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        ChatRoom chatRoom = new ChatRoom("chatroom");
        chatRoom.addUser(user);
        assertEquals(messageService.addMessage(new NewMessageTransport("hello")).getResponse(), "Message sent!");
    }

    @Test(expected = MessageException.class)
    public void send_null_last_time_fetched_request_for_unread() throws MessageException, UserException {
        messageService.getUnread(0);
    }

    @Test
    public void empty_new_messages_on_request_for_unread() throws MessageException, UserException {
        User user = new User("username", "password");
        TokenHandler.setToken(user.getToken());
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        ChatRoom chatRoom = new ChatRoom("chatroom");
        chatRoom.addUser(user);
        assertEquals(messageService.getUnread(System.currentTimeMillis()).getMessages().size(), 0);
    }

    @Test
    public void new_messages_on_request_for_unread() throws MessageException, UserException {
        User user = new User("username", "password");
        TokenHandler.setToken(user.getToken());
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        ChatRoom chatRoom = new ChatRoom("chatroom");
        chatRoom.addUser(user);
        chatRoom.addMessage(new Message("new message", user, new Timestamp(System.currentTimeMillis() + 5000)));
        assertEquals(messageService.getUnread(System.currentTimeMillis()).getMessages().size(), 1);
    }
}
