package unit;

import chatapp.TokenHandler;
import chatapp.entities.chatroom.ChatRoom;
import chatapp.entities.chatroom.ChatRoomException;
import chatapp.entities.dto.chatroom.ChatRoomNamesListTransport;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.ChatRoomRepo;
import chatapp.repos.UserRepo;
import chatapp.services.ChatRoomService;
import chatapp.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChatRoomServiceTests {

    @Spy
    UserRepo userRepo;

    @Spy
    @InjectMocks
    UserService userService = new UserService();

    @Spy
    ChatRoomRepo chatRoomRepo;

    @InjectMocks
    ChatRoomService chatRoomService;

    @Test
    public void test_get_rooms_list_request() {
        List<ChatRoom> rooms = Arrays.asList(new ChatRoom("chatroom"), new ChatRoom("room"));
        when(chatRoomRepo.findAll()).thenReturn(rooms);
        assertEquals(chatRoomService.getRooms(), rooms);
    }

    @Test
    public void test_get_room_names_list_request() {
        List<ChatRoom> rooms = Arrays.asList(new ChatRoom("chatroom"), new ChatRoom("room"));
        when(chatRoomRepo.findAll()).thenReturn(rooms);
        ChatRoomNamesListTransport namesListTransport = new ChatRoomNamesListTransport(rooms);
        assertEquals(chatRoomService.getRoomNames().getNames(), namesListTransport.getNames());
    }

    @Test
    public void test_add_user_to_room_request() throws UserException, ChatRoomException {
        User user = new User("username", "password");
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        ChatRoom chatRoom = new ChatRoom("chatroom");
        when(chatRoomRepo.findById(anyString())).thenReturn(Optional.of(chatRoom));
        chatRoom.addUser(user);
        TokenHandler.setToken(user.getToken());
        assertTrue(chatRoomService.addUserToRoom("any").getMessages().stream().allMatch(m -> m.getContent().equals(" has joined the room.")));
    }

    @Test(expected = ChatRoomException.class)
    public void test_add_user_to_null_room_name() throws ChatRoomException, UserException {
        chatRoomService.addUserToRoom(null);
    }

    @Test(expected = ChatRoomException.class)
    public void test_add_user_to_empty_room_name() throws ChatRoomException, UserException {
        chatRoomService.addUserToRoom("    ");
    }

    @Test(expected = ChatRoomException.class)
    public void test_add_user_to_non_existing_room() throws ChatRoomException, UserException {
        when(chatRoomRepo.findById(anyString())).thenReturn(Optional.empty());
        chatRoomService.addUserToRoom("room");
    }

    @Test
    public void user_removal_from_room() throws UserException {
        User user = new User("username", "password");
        TokenHandler.setToken(user.getToken());
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        ChatRoom chatRoom = new ChatRoom("chatroom");
        chatRoom.addUser(user);
        assertEquals(chatRoomService.removeUserFromRoom().getResponse(), user.getUsername() + " has left the room.");
    }

    @Test(expected = ChatRoomException.class)
    public void test_null_room_name_room_creation_request() throws ChatRoomException, UserException {
        chatRoomService.createRoom(null);
    }

    @Test(expected = ChatRoomException.class)
    public void test_empty_room_name_room_creation_request() throws ChatRoomException, UserException {
        chatRoomService.createRoom("    ");
    }

    @Test(expected = ChatRoomException.class)
    public void test_invalid_room_name_format_on_room_creation_request() throws ChatRoomException, UserException {
        chatRoomService.createRoom("...");
    }

    @Test
    public void valid_room_creation_request() throws UserException, ChatRoomException {
        User user = new User("username", "password");
        TokenHandler.setToken(user.getToken());
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        assertEquals(chatRoomService.createRoom("test").getRoomName(), "test");
    }


}
