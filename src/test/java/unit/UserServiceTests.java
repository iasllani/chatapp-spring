package unit;

import chatapp.entities.dto.authentication.AuthenticationRequestTransport;
import chatapp.entities.dto.authentication.AuthenticationTokenTransport;
import chatapp.entities.dto.registration.RegistrationRequestTransport;
import chatapp.entities.dto.registration.RegistrationResponseTransport;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.UserRepo;
import chatapp.services.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    @Mock
    UserRepo userRepo;

    @InjectMocks
    UserService userService;

    @Test
    public void test_valid_user_registration() throws UserException {
        RegistrationRequestTransport requestTransport = new RegistrationRequestTransport("username", "password");
        RegistrationResponseTransport response = userService.registerUser(requestTransport);
        when(userRepo.findById(anyInt())).thenReturn(Optional.empty());
        assertEquals(response.getResponse(), "Successfully registered");
    }

    @Test(expected = UserException.BadUsernameFormat.class)
    public void test_invalid_username_on_registration() throws UserException {
        RegistrationRequestTransport requestTransport = new RegistrationRequestTransport("....", "password");
        userService.registerUser(requestTransport);
    }

    @Test(expected = UserException.BadPasswordFormat.class)
    public void test_invalid_password_on_registration() throws UserException {
        RegistrationRequestTransport requestTransport = new RegistrationRequestTransport("username", "....");
        userService.registerUser(requestTransport);
    }

    @Test(expected = UserException.class)
    public void test_empty_request_on_registration() throws UserException {
        userService.registerUser(new RegistrationRequestTransport());
    }

    @Test(expected = UserException.class)
    public void test_null_request_on_registration() throws UserException {
        userService.registerUser(null);
    }

    @Test(expected = UserException.class)
    public void test_no_username_provided_request_on_registration() throws UserException {
        userService.registerUser(new RegistrationRequestTransport(null, "password"));
    }

    @Test(expected = UserException.class)
    public void test_no_password_provided_request_on_registration() throws UserException {
        userService.registerUser(new RegistrationRequestTransport("username", null));
    }

    @Test
    public void test_valid_authentication_request() throws UserException {
        User user = new User("username", "password");
        when(userRepo.findByUsernameAndPassword(anyString(), anyString())).thenReturn(user);
        AuthenticationTokenTransport tokenTransport = userService.authenticateUser(new AuthenticationRequestTransport("username", "password"));
        assertEquals(tokenTransport.getToken(), (int) user.getToken());
    }

    @Test(expected = UserException.class)
    public void test_invalid_authentication_request() throws UserException {
        when(userRepo.findByUsernameAndPassword(anyString(), anyString())).thenReturn(null);
        userService.authenticateUser(new AuthenticationRequestTransport("username", "password"));
    }

    @Test(expected = UserException.class)
    public void test_no_username_provided_request_on_authentication() throws UserException {
        userService.authenticateUser(new AuthenticationRequestTransport(null, "password"));
    }

    @Test(expected = UserException.class)
    public void test_no_password_provided_request_on_authentication() throws UserException {
        userService.authenticateUser(new AuthenticationRequestTransport("username", null));
    }

    @Test(expected = UserException.class)
    public void test_bad_username_format_provided_request_on_authentication() throws UserException {
        userService.authenticateUser(new AuthenticationRequestTransport("....", "password"));
    }

    @Test(expected = UserException.class)
    public void test_bad_password_format_provided_request_on_authentication() throws UserException {
        userService.authenticateUser(new AuthenticationRequestTransport("username", "...."));
    }

    @Test(expected = UserException.class)
    public void test_empty_request_on_authentication() throws UserException {
        userService.authenticateUser(new AuthenticationRequestTransport());
    }

    @Test(expected = UserException.class)
    public void test_null_request_on_authentication() throws UserException {
        userService.authenticateUser(null);
    }

    @Test
    public void test_get_users_list_request() {
        List<User> users = Arrays.asList(new User("username", "password"), new User("ilir", "password"));
        when(userRepo.findAll()).thenReturn(users);
        assertEquals(userService.getUsers(), users);
    }

    @Test
    public void test_get_user_based_on_token() throws UserException {
        User user = new User("username", "password");
        when(userRepo.findById(anyInt())).thenReturn(java.util.Optional.of(user));
        assertEquals(userService.getUser(1), user);
    }

    @Test(expected = UserException.class)
    public void test_get_user_when_given_null_request() throws UserException {
        when(userRepo.findById(anyInt())).thenReturn(Optional.empty());
        userService.getUser(1);
    }


}