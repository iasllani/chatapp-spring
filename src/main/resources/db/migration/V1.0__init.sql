DROP TABLE IF EXISTS `chat_room`;

CREATE TABLE `chat_room` (
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `token` int(32) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `room_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`token`),
  UNIQUE KEY `username` (`username`),
  CONSTRAINT `FK_userRoom` FOREIGN KEY (`room_name`) REFERENCES `chat_room`(`name`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` Timestamp(3) NOT NULL,
  `content` varchar(256) NOT NULL,
  `sender_token` int(32) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),	
  CONSTRAINT `FK_senderToken` FOREIGN KEY (`sender_token`) REFERENCES `user`(`token`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,	
  CONSTRAINT `FK_messageRoom` FOREIGN KEY (`room_name`) REFERENCES `chat_room`(`name`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

	
DROP TABLE IF EXISTS `user_room_state`;

CREATE TABLE `user_room_state`(
  `id` int(32) NOT NULL,
  `user_token` int(32) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  `last_fetch` Timestamp(3) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_userToken` FOREIGN KEY(`user_token`) REFERENCES `user`(`token`),
  CONSTRAINT `FK_roomName` FOREIGN KEY (`room_name`) REFERENCES `chat_room`(`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 


