package chatapp;


public class TokenHandler {

    private static ThreadLocal<Integer> token = new ThreadLocal<>();

    public static Integer getToken() {
        return token.get();
    }

    public static void setToken(Integer newToken) {
        token.set(newToken);
    }

}