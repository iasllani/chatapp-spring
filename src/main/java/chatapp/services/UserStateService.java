package chatapp.services;

import chatapp.entities.UserRoomState;
import chatapp.entities.chatroom.ChatRoom;
import chatapp.repos.UserStateRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserStateService {

    private UserStateRepo userStateRepo;

    public UserStateService(UserStateRepo userStateRepo){
        this.userStateRepo = userStateRepo;
    }

    public List<UserRoomState> findUserStatesByRoom(String roomName){
        return userStateRepo.findAll().stream().filter(state -> state.getChatRoom().getName().equals(roomName)).collect(Collectors.toList());
    }
}
