package chatapp.services;

import chatapp.TokenHandler;
import chatapp.entities.UserRoomState;
import chatapp.entities.chatroom.ChatRoom;
import chatapp.entities.chatroom.ChatRoomException;
import chatapp.entities.dto.chatroom.ChatRoomNameTransport;
import chatapp.entities.dto.chatroom.ChatRoomNamesListTransport;
import chatapp.entities.dto.chatroom.ChatRoomResponseTransport;
import chatapp.entities.dto.deletion.RoomDeletionResponseTransport;
import chatapp.entities.dto.message.MessageListTransport;
import chatapp.entities.message.Message;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.ChatRoomRepo;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChatRoomService {

    private final ChatRoomRepo chatRoomRepo;
    private final UserService userService;
    private final MessageService messageService;
    private final UserStateService userStateService;

    public ChatRoomService(ChatRoomRepo chatRoomRepo, UserService userService, MessageService messageService,UserStateService userStateService) {
        this.chatRoomRepo = chatRoomRepo;
        this.userService = userService;
        this.messageService = messageService;
        this.userStateService = userStateService;
    }

    public List<ChatRoom> getRooms() {
        return chatRoomRepo.findAll();
    }

    private void saveRoom(ChatRoom room) {
        chatRoomRepo.save(room);
    }

    private ChatRoom getRoom(String name) {
        return chatRoomRepo.findById(name).orElse(null);
    }

    public ChatRoomNamesListTransport getRoomNames() {
        return new ChatRoomNamesListTransport(this.getRooms());
    }

    public MessageListTransport addUserToRoom(String roomName) throws ChatRoomException, UserException {
        if (roomName == null)
            throw new ChatRoomException.ChatRoomNotFound("Invalid room name.");
        if (roomName.trim().isEmpty())
            throw new ChatRoomException.ChatRoomNotFound("Empty roomName");
        ChatRoom room = this.getRoom(roomName);
        if (room == null)
            throw new ChatRoomException.ChatRoomNotFound("Couldn't find a room that matches the given name.");
        User user = userService.getUser(TokenHandler.getToken());
        Timestamp lastFetch = room.addUser(user);
        room.addMessage(new Message(" has joined the room.", user, new Timestamp(System.currentTimeMillis())));
        userService.saveUserState(user);
        List<Message> messages = room.getMessages().stream().filter(m -> m.getTimestamp().getTime() > lastFetch.getTime()).collect(Collectors.toList());
        if (messages.size() != 0) {
            user.getUserRoomStates().stream().filter(state -> state.getChatRoom() == user.getChatRoom()).findFirst()
                    .ifPresent(userRoomState -> userRoomState.setLastFetch(messages.get(messages.size() - 1).getTimestamp()));
            userService.saveUserState(user);
        }
        return new MessageListTransport(messages);
    }

    public ChatRoomResponseTransport removeUserFromRoom() throws UserException {
        User user = userService.getUser(TokenHandler.getToken());
        user.getChatRoom().addMessage(new Message(" has left the room", user, new Timestamp(System.currentTimeMillis())));
        user.getChatRoom().removeUser(user);
        userService.saveUserState(user);
        return new ChatRoomResponseTransport(user.getUsername() + " has left the room.");
    }

    public ChatRoomNameTransport createRoom(String roomName) throws ChatRoomException, UserException {
        if (roomName == null)
            throw new ChatRoomException.BadChatRoomNameFormat("Invalid room name.");
        if (roomName.trim().isEmpty())
            throw new ChatRoomException.BadChatRoomNameFormat("Empty room name.");
        if (!roomName.matches("^[a-zA-Z0-9]+"))
            throw new ChatRoomException.BadChatRoomNameFormat("Room name can contain letters or numbers only.");
        User user = userService.getUser(TokenHandler.getToken());
        ChatRoom room = new ChatRoom(roomName);
        room.addMessage(new Message(" created the room.", user, new Timestamp(System.currentTimeMillis())));
        this.saveRoom(room);
        return new ChatRoomNameTransport(roomName);
    }

    public RoomDeletionResponseTransport deleteRoom(String roomName) throws ChatRoomException, UserException {
        if (roomName == null)
            throw new ChatRoomException.BadChatRoomNameFormat("Invalid room name.");
        if (roomName.trim().isEmpty())
            throw new ChatRoomException.BadChatRoomNameFormat("Empty room name.");
        if (!roomName.matches("^[a-zA-Z0-9]+"))
            throw new ChatRoomException.BadChatRoomNameFormat("Room name can contain letters or numbers only.");
        userService.getUser(TokenHandler.getToken());
        chatRoomRepo.deleteById(roomName);
        return new RoomDeletionResponseTransport(roomName);
    }

    public void deleteReadMessages() {
        List<ChatRoom> rooms = this.getRooms();
        rooms.forEach(this::deleteRead);
    }

    public void deleteRead(ChatRoom room) {
        UserRoomState userState = userStateService.findUserStatesByRoom(room.getName()).stream().min(Comparator.comparingLong(state -> state.getLastFetch().getTime())).orElse(null);
        if (userState != null) {
            deleteMessages(room.getMessages()
                    .stream().filter(message -> message.getTimestamp().getTime() < userState.getLastFetch().getTime())
                    .collect(Collectors.toList()));
        }
    }

    private void deleteMessages(List<Message> toDelete) {
        messageService.deleteMessages(toDelete);
    }
}
