package chatapp.services;

import chatapp.entities.dto.authentication.AuthenticationRequestTransport;
import chatapp.entities.dto.authentication.AuthenticationTokenTransport;
import chatapp.entities.dto.deletion.UserDeletionResponseTransport;
import chatapp.entities.dto.registration.RegistrationRequestTransport;
import chatapp.entities.dto.registration.RegistrationResponseTransport;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserService() {
    }

    public List<User> getUsers() {
        return userRepo.findAll();
    }

    public AuthenticationTokenTransport authenticateUser(AuthenticationRequestTransport requestTransport) throws UserException {
        if (requestTransport == null)
            throw new UserException("Are you missing something?");
        if (requestTransport.getUsername() == null)
            throw new UserException.BadUsernameFormat("No username provided");
        if (requestTransport.getPassword() == null)
            throw new UserException.BadUsernameFormat("No password provided");
        String username = requestTransport.getUsername();
        if (!username.matches("^[a-zA-Z0-9]+"))
            throw new UserException.BadUsernameFormat("Invalid format for username. Can only contain numbers and letters.");
        String password = requestTransport.getPassword();
        if (!password.matches("^[a-zA-Z0-9]+"))
            throw new UserException.BadPasswordFormat("Invalid format for password. Can only contain numbers and letters.");
        User user = userRepo.findByUsernameAndPassword(username, password);
        if (user == null)
            throw new UserException.NotFound("Incorrect username or password");
        return new AuthenticationTokenTransport(user.getToken());
    }

    public RegistrationResponseTransport registerUser(RegistrationRequestTransport requestTransport) throws UserException {
        if (requestTransport == null || requestTransport.getUsername() == null || requestTransport.getPassword() == null)
            throw new UserException("Are you missing something?");
        String username = requestTransport.getUsername();
        if (!username.matches("^[a-zA-Z0-9]+"))
            throw new UserException.BadUsernameFormat("Invalid format for username. Can only contain numbers and letters.");
        String password = requestTransport.getPassword();
        if (!password.matches("^[a-zA-Z0-9]+"))
            throw new UserException.BadPasswordFormat("Invalid format for password. Can only contain numbers and letters.");
        User user = new User(username, password);
        this.saveUser(user);
        return new RegistrationResponseTransport("Successfully registered");
    }

    public AuthenticationTokenTransport saveUser(User user) throws UserException {
        if (userRepo.findById(user.getUsername().hashCode()).orElse(null) != null)
            throw new UserException.AlreadyExists("Someone has already taken this username");
        userRepo.save(user);
        return new AuthenticationTokenTransport(user.getToken());
    }

    public void saveUserState(User user){
        userRepo.save(user);
    }

    public User getUser(int token) throws UserException {
        User user = userRepo.findById(token).orElse(null);
        if (user == null)
            throw new UserException.CompromisedToken("User token compromised");
        return user;
    }

    public UserDeletionResponseTransport deleteUser(Integer token) {
        userRepo.deleteById(token);
        return new UserDeletionResponseTransport("Successfully deleted");

    }

}
