package chatapp.services;

import chatapp.TokenHandler;
import chatapp.entities.UserRoomState;
import chatapp.entities.dto.message.MessageListTransport;
import chatapp.entities.dto.message.MessageReceivedTransport;
import chatapp.entities.dto.message.NewMessageTransport;
import chatapp.entities.message.Message;
import chatapp.entities.message.MessageException;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.repos.MessageRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    private final MessageRepo messageRepo;
    private final UserService userService;

    public MessageService(MessageRepo messageRepo, UserService userService) {
        this.messageRepo = messageRepo;
        this.userService = userService;
    }

    private void saveMessage(Message message) {
        message.getSender().getChatRoom().addMessage(message);
        messageRepo.save(message);
    }

    public MessageReceivedTransport addMessage(NewMessageTransport messageTransport) throws MessageException, UserException {
        if (messageTransport == null)
            throw new MessageException("Are you missing something?");
        if (messageTransport.getContent() == null)
            throw new MessageException.BadContent("Invalid content");
        if (messageTransport.getContent().trim().isEmpty())
            throw new MessageException.BadContent("Empty message");
        if (messageTransport.getContent().length() > 256)
            throw new MessageException.BadContent("Message length can't be longer than 256 characters");
        User user = userService.getUser(TokenHandler.getToken());
        Message message = new Message(messageTransport, user);
        this.saveMessage(message);
        return new MessageReceivedTransport("Message sent!");
    }

    public MessageListTransport getUnread(long lastFetched) throws UserException, MessageException {
        if (lastFetched == 0)
            throw new MessageException("Are you missing something");
        User user = userService.getUser(TokenHandler.getToken());
        List<Message> messages = user.getChatRoom().getMessages().stream().filter(m -> m.getTimestamp().getTime() > lastFetched).collect(Collectors.toList());
        if (messages.size() != 0) {
            user.getUserRoomStates().stream().filter(state -> state.getChatRoom() == user.getChatRoom()).findFirst()
                    .ifPresent(userRoomState -> userRoomState.setLastFetch(messages.get(messages.size() - 1).getTimestamp()));
            userService.saveUserState(user);
        }
        return new MessageListTransport(messages);
    }

    public void deleteMessages(List<Message> toDelete){
        messageRepo.deleteAll(toDelete);
    }
}
