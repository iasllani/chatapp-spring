package chatapp.entities.message;

public class MessageException extends Exception {

    public MessageException(String msg) {
        super(msg);
    }


    public static class BadContent extends MessageException {

        public BadContent(String msg) {
            super(msg);
        }
    }

    public static class BadTimestamp extends MessageException {
        public BadTimestamp(String msg) {
            super(msg);
        }
    }

}
