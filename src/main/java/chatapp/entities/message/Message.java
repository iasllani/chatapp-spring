package chatapp.entities.message;

import chatapp.entities.chatroom.ChatRoom;
import chatapp.entities.dto.message.NewMessageTransport;
import chatapp.entities.user.User;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "message")
public class Message {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Timestamp timestamp;

    @OneToOne(fetch = FetchType.LAZY, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "senderToken")
    private User sender;

    @Column
    private String content;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "roomName")
    private ChatRoom chatRoom;

    public Message() {
    }

    public Message(NewMessageTransport messageTransport, User sender) {
        this.timestamp = messageTransport.getTimestamp();
        this.content = messageTransport.getContent();
        this.sender = sender;
    }

    public Message(String content, User sender, Timestamp timestamp) {
        this.timestamp = timestamp;
        this.sender = sender;
        this.content = content;
    }

    public Message(String content) {
        this.content = content;
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    @CreationTimestamp
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    @Override
    public String toString() {
        return sender + ": \n" + content;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Message && ((Message) o).getSender() == this.getSender() && ((Message) o).getTimestamp() == this.getTimestamp() && ((Message) o).getContent().equals(this.getContent());
    }
}
