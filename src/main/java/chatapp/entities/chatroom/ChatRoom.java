package chatapp.entities.chatroom;

import chatapp.entities.UserRoomState;
import chatapp.entities.message.Message;
import chatapp.entities.user.User;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "chat_room")
public class ChatRoom {

    @Id
    @Column
    private String name;

    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "chatRoom", cascade = CascadeType.ALL)
    private List<Message> messages;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "chatRoom",
            cascade = {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            })
    private List<User> users;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "chatRoom",
            cascade = {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            })
    private List<UserRoomState> userRoomStates;

    public ChatRoom() {
    }

    public ChatRoom(String name) {
        this.name = name;
        this.messages = new ArrayList<>();
    }


    public Timestamp addUser(User user) {
        if (this.users == null)
            this.users = new ArrayList<>();
        UserRoomState userRoomState = userRoomStates.stream().filter(state -> state.getUser().equals(user)).findFirst().orElse(null);
        if (userRoomState != null) {
            user.setChatRoom(this);
            return userRoomStates.stream().filter(state -> state.getUser().equals(user)).findFirst().orElse(null).getLastFetch();
        }
        user.setChatRoom(this);
        this.users.add(user);
        userRoomState = new UserRoomState(this, user);
        if (userRoomStates == null)
            userRoomStates = new ArrayList<>();
        this.userRoomStates.add(userRoomState);
        user.addState(userRoomState);
        return userRoomStates.stream().filter(state -> state.getUser().equals(user)).
                findFirst().orElse(null).getLastFetch();
    }

    public void removeUser(User user) {
        user.setChatRoom(null);
    }

    //room knows the last time a user fetched the messages
    //room has a list corresponding with the last time a user fetched the messages of that room.
    public void addMessage(Message message) {
        if (this.messages == null)
            this.messages = new ArrayList<>();
        message.setChatRoom(this);
        this.messages.add(message);
    }

    public List<UserRoomState> getUserRoomStates() {
        return userRoomStates;
    }

    public void setUserRoomStates(List<UserRoomState> userRoomStates) {
        this.userRoomStates = userRoomStates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean equals(Object o) {
        return o instanceof ChatRoom && ((ChatRoom) o).getName().equals(this.getName());
    }

    @Override
    public String toString() {
        return "chatroom{" +
                "name='" + name + '\'' +
                '}';
    }
}
