package chatapp.entities.chatroom;

public class ChatRoomException extends Exception {

    public ChatRoomException(String msg) {
        super(msg);
    }

    public static class BadChatRoomNameFormat extends ChatRoomException {

        public BadChatRoomNameFormat(String msg) {
            super(msg);
        }
    }

    public static class ChatRoomNotFound extends ChatRoomException {

        public ChatRoomNotFound(String msg) {
            super(msg);
        }
    }
}
