package chatapp.entities;

import chatapp.entities.chatroom.ChatRoom;
import chatapp.entities.user.User;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "user_room_state")
public class UserRoomState implements Serializable {

    @Id
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "room_name")
    private ChatRoom chatRoom;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_token")
    private User user;

    @Column
    private Timestamp lastFetch;

    public UserRoomState() {
    }

    public UserRoomState(ChatRoom chatRoom, User user) {
        this.chatRoom = chatRoom;
        this.user = user;
        this.lastFetch = new Timestamp(1000 * 60 * 60 * 2);
        this.id = this.hashCode();
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getLastFetch() {
        return lastFetch;
    }

    public void setLastFetch(Timestamp lastFetch) {
        this.lastFetch = lastFetch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof UserRoomState && ((UserRoomState) o).getUser().equals(this.getUser()) && ((UserRoomState) o).getChatRoom().equals(this.getChatRoom());
    }

    @Override
    public int hashCode() {
        return this.getChatRoom().toString().hashCode() + this.getUser().toString().hashCode();
    }

}
