package chatapp.entities.dto.chatroom;

import chatapp.entities.chatroom.ChatRoom;

import java.util.ArrayList;
import java.util.List;

public class ChatRoomNamesListTransport {

    private List<String> names;

    public ChatRoomNamesListTransport() {
        names = new ArrayList<>();

    }

    public ChatRoomNamesListTransport(List<ChatRoom> rooms) {
        names = new ArrayList<>();
        rooms.forEach(r -> this.names.add(r.getName()));
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public void printRoomNames() {
        names.forEach(System.out::println);
    }
}
