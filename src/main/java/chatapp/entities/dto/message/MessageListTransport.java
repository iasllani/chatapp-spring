package chatapp.entities.dto.message;

import chatapp.entities.message.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageListTransport {

    private String roomName;

    private List<MessageTransport> messages;

    public MessageListTransport() {
    }

    public MessageListTransport(List<Message> messages) {
        this.messages = new ArrayList<>();
        messages.stream().forEach(m -> this.messages.add(new MessageTransport(m)));
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public List<MessageTransport> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageTransport> messages) {
        this.messages = messages;
    }

    public void printMessages() {
        messages.forEach(System.out::println);
    }
}
