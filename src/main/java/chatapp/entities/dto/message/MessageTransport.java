package chatapp.entities.dto.message;

import chatapp.entities.message.Message;

import java.sql.Timestamp;

public class MessageTransport {

    private String senderName;

    private Timestamp timestamp;

    private String content;

    public MessageTransport() {
    }

    public MessageTransport(Message message) {
        this.senderName = message.getSender().getUsername();
        this.timestamp = message.getTimestamp();
        this.content = message.getContent();
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @Override
    public String toString() {
        return senderName + ":\n" + "[" + timestamp + "]  " + content;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof MessageTransport &&
                ((MessageTransport) o).getSenderName().equals(this.getSenderName()) &&
                ((MessageTransport) o).getContent().equals(this.getContent()) &&
                ((MessageTransport) o).getTimestamp() == this.getTimestamp();
    }

}
