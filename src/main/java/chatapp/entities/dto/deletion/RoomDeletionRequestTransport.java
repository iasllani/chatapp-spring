package chatapp.entities.dto.deletion;

public class RoomDeletionRequestTransport {

    private String roomName;

    public RoomDeletionRequestTransport() {
    }

    public RoomDeletionRequestTransport(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
