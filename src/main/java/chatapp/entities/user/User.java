package chatapp.entities.user;

import chatapp.entities.UserRoomState;
import chatapp.entities.chatroom.ChatRoom;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "token")
    private Integer token;

    @Column
    private String username;

    @Column
    private String password;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "roomName")
    private ChatRoom chatRoom;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "user",
            cascade = {
                    CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH
            })
    private List<UserRoomState> userRoomStates;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.token = this.username.hashCode();
    }

    public void addState(UserRoomState userRoomState) {
        if (userRoomStates == null)
            userRoomStates = new ArrayList<>();
        userRoomStates.add(userRoomState);
    }


    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRoomState> getUserRoomStates() {
        return userRoomStates;
    }

    public void setUserRoomStates(List<UserRoomState> userRoomStates) {
        this.userRoomStates = userRoomStates;
    }

    @Override
    public boolean equals(Object o) {
        return o == this || o instanceof User && ((User) o).getUsername().equals(this.getUsername());
    }


    @Override
    public String toString() {
        return this.username;
    }
}
