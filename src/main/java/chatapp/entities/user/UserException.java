package chatapp.entities.user;

public class UserException extends Exception {

    public UserException(String msg) {
        super(msg);
    }

    public static class BadUsernameFormat extends UserException {
        public BadUsernameFormat(String msg) {
            super(msg);
        }
    }

    public static class BadPasswordFormat extends UserException {
        public BadPasswordFormat(String msg) {
            super(msg);
        }
    }

    public static class NoAuthenticationProvided extends UserException {
        public NoAuthenticationProvided(String msg) {
            super(msg);
        }
    }

    public static class NotFound extends UserException {
        public NotFound(String msg) {
            super(msg);
        }
    }

    public static class CompromisedToken extends UserException {
        public CompromisedToken(String msg) {
            super(msg);
        }
    }

    public static class AlreadyExists extends UserException {
        public AlreadyExists(String msg) {
            super(msg);
        }
    }
}
