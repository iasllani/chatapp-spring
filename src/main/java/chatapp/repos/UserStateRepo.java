package chatapp.repos;

import chatapp.entities.UserRoomState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserStateRepo extends JpaRepository<UserRoomState,Integer> {
}
