package chatapp.config;

import chatapp.services.ChatRoomService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MonthlyMessageCleanerScheduler {

    private ThreadPoolTaskScheduler taskScheduler;
    private ChatRoomService chatRoomService;

    public MonthlyMessageCleanerScheduler(ChatRoomService chatRoomService,ThreadPoolTaskScheduler taskScheduler) {
        this.chatRoomService = chatRoomService;
        this.taskScheduler = taskScheduler;
    }

    @PostConstruct
    public void deleteReadMessages() {
        taskScheduler.scheduleAtFixedRate(
                () -> chatRoomService.deleteReadMessages(),
                20000);
    }

}
