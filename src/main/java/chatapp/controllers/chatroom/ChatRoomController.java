package chatapp.controllers.chatroom;

import chatapp.entities.chatroom.ChatRoomException;
import chatapp.entities.dto.chatroom.ChatRoomNameTransport;
import chatapp.entities.dto.chatroom.ChatRoomNamesListTransport;
import chatapp.entities.dto.chatroom.ChatRoomResponseTransport;
import chatapp.entities.dto.deletion.RoomDeletionResponseTransport;
import chatapp.entities.dto.message.MessageListTransport;
import chatapp.entities.user.UserException;
import chatapp.services.ChatRoomService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/rooms")
public class ChatRoomController {

    private final ChatRoomService chatRoomService;


    public ChatRoomController(ChatRoomService chatRoomService) {
        this.chatRoomService = chatRoomService;
    }

    @GetMapping
    public ChatRoomNamesListTransport getAllRooms() {
        return chatRoomService.getRoomNames();
    }

    @GetMapping("/create/{roomName}")
    public ChatRoomNameTransport createRoom(@PathVariable String roomName) throws ChatRoomException, UserException {
        return chatRoomService.createRoom(roomName);
    }

    @GetMapping("/join/{roomName}")
    public MessageListTransport joinRoom(@PathVariable String roomName) throws UserException, ChatRoomException {
        return chatRoomService.addUserToRoom(roomName);
    }

    @GetMapping("/leave")
    public ChatRoomResponseTransport leaveRoom() throws UserException {
        return chatRoomService.removeUserFromRoom();
    }

    @GetMapping("/delete/{roomName}")
    public RoomDeletionResponseTransport deleteRoom(@PathVariable String roomName) throws ChatRoomException, UserException {
        return chatRoomService.deleteRoom(roomName);
    }
}
