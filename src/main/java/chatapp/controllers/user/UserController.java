package chatapp.controllers.user;

import chatapp.TokenHandler;
import chatapp.entities.dto.authentication.AuthenticationRequestTransport;
import chatapp.entities.dto.authentication.AuthenticationTokenTransport;
import chatapp.entities.dto.deletion.UserDeletionResponseTransport;
import chatapp.entities.dto.registration.RegistrationRequestTransport;
import chatapp.entities.dto.registration.RegistrationResponseTransport;
import chatapp.entities.user.UserException;
import chatapp.services.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/authenticate")
    public AuthenticationTokenTransport authenticateUser(@RequestBody AuthenticationRequestTransport requestTransport) throws UserException {
        return userService.authenticateUser(requestTransport);
    }

    @PostMapping("/register")
    public RegistrationResponseTransport registerUser(@RequestBody RegistrationRequestTransport requestTransport) throws UserException {
        return userService.registerUser(requestTransport);
    }

    @DeleteMapping("/delete")
    public UserDeletionResponseTransport deleteUser() throws UserException {
        return userService.deleteUser(TokenHandler.getToken());
    }

}
