package chatapp.controllers.user;

import chatapp.controllers.ChatErrorResponse;
import chatapp.entities.user.UserException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class UserExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ChatErrorResponse> handleException(UserException exc) {
        ChatErrorResponse errorResponse = new ChatErrorResponse();
        HttpStatus httpResponse = null;
        if (exc instanceof UserException.BadUsernameFormat)
            httpResponse = HttpStatus.BAD_REQUEST;
        else if (exc instanceof UserException.NoAuthenticationProvided)
            httpResponse = HttpStatus.UNAUTHORIZED;
        else if (exc instanceof UserException.NotFound)
            httpResponse = HttpStatus.UNAUTHORIZED;

        errorResponse.setStatus(httpResponse.value());
        errorResponse.setMessage(exc.getMessage());
        errorResponse.setDate(new Date().toString());

        return new ResponseEntity<>(errorResponse, httpResponse);
    }

    @ExceptionHandler
    public ResponseEntity<ChatErrorResponse> handleException(Exception e) {
        ChatErrorResponse errorResponse = new ChatErrorResponse();
        HttpStatus httpResponse = HttpStatus.BAD_REQUEST;
        errorResponse.setStatus(httpResponse.value());
        errorResponse.setMessage(e.getMessage());
        errorResponse.setDate(new Date().toString());

        return new ResponseEntity<>(errorResponse, httpResponse);
    }
}
