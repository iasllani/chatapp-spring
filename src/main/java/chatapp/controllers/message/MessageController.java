package chatapp.controllers.message;

import chatapp.entities.dto.message.MessageListTransport;
import chatapp.entities.dto.message.MessageReceivedTransport;
import chatapp.entities.dto.message.NewMessageTransport;
import chatapp.entities.message.MessageException;
import chatapp.entities.user.UserException;
import chatapp.services.MessageService;
import chatapp.services.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    public MessageController(UserService userService, MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/unread")
    public MessageListTransport getUnread(@RequestBody long lastFetched) throws UserException, MessageException {
        return messageService.getUnread(lastFetched);
    }

    @PostMapping("/send")
    public MessageReceivedTransport addMessage(@RequestBody NewMessageTransport message) throws UserException, MessageException {
        return messageService.addMessage(message);
    }
}
