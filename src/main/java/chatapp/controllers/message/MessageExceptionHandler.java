package chatapp.controllers.message;

import chatapp.controllers.ChatErrorResponse;
import chatapp.entities.chatroom.ChatRoomException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

public class MessageExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ChatErrorResponse> handleException(ChatRoomException exc) {
        ChatErrorResponse errorResponse = new ChatErrorResponse();
        HttpStatus httpResponse = null;
        if (exc instanceof ChatRoomException.BadChatRoomNameFormat)
            httpResponse = HttpStatus.BAD_REQUEST;
        errorResponse.setStatus(httpResponse.value());
        errorResponse.setMessage(exc.getMessage());
        errorResponse.setDate(new Date().toString());

        return new ResponseEntity<>(errorResponse, httpResponse);
    }

    @ExceptionHandler
    public ResponseEntity<ChatErrorResponse> handleException(Exception e) {
        ChatErrorResponse errorResponse = new ChatErrorResponse();
        HttpStatus httpResponse = HttpStatus.BAD_REQUEST;
        errorResponse.setStatus(httpResponse.value());
        errorResponse.setMessage(e.getMessage());
        errorResponse.setDate(new Date().toString());

        return new ResponseEntity<>(errorResponse, httpResponse);
    }
}
