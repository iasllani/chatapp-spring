package chatapp.filters;

import chatapp.TokenHandler;
import chatapp.entities.user.User;
import chatapp.entities.user.UserException;
import chatapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String authorization = request.getHeader("Authorization");
        if (authorization == null) {
            response.sendError(401, "You have not been authenticated");
            return;
        } else if (!authorization.startsWith("Bearer") || !authorization.split(" ")[1].matches("^-?[0-9]+")) {
            response.sendError(401, "Bad authentication");
            return;
        }
        User user;
        try {
            user = userService.getUser(Integer.parseInt(authorization.split(" ")[1]));
        } catch (UserException e) {
            response.sendError(400, e.getMessage());
            return;
        }
        if (user == null) {
            response.sendError(401, "You have not been authenticated");
            return;
        }
        TokenHandler.setToken(user.getToken());
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return request.getRequestURI().startsWith("/users/register") || request.getRequestURI().startsWith("/users/authenticate");
    }
}
